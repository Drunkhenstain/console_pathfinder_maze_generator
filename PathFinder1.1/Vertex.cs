﻿using System;
namespace PathFinder1
{
	public class Vertex:IComparable
	{
		String id;
		int value;

		int posX;
		int posY;

		public int getX()
		{
			return posX;
		}

		public int getY()
		{
			return posY;
		}

		public int getValue()
		{
			return value;
		}

		public string getID()
		{
			return id;
		}

		public int CompareTo(object obj)
		{
			throw new NotImplementedException();
		}

		public Vertex(int posX,int posY,int value)
		{
			id += posX;
            id += ".";
			id += posY;
			this.posX = posX;
			this.posY = posY;
			this.value = value;
		}
	}
}

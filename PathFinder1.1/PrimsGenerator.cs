﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathFinder1
{
    public class PrimsGenerator
    {

        List<Cell> walls = new List<Cell>();
        List<Cell> cells = new List<Cell>();
        static Random random = new Random();
        char[,] map;

        public char[,] MazeIt(int width, int heigth)
        {
            // Maze Generator by Charles-William Crete
            // More info: http://en.wikipedia.org/wiki/Maze_generation_algorithm#Randomized_Prim.27s_algorithm
            // Released on the GPL Lisence (http://www.gnu.org/copyleft/gpl.html)
            int max = 150000;
            int widthmap = width;
            int heigthmap = heigth;
            map = new char[width, heigth];
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < heigth; y++)
                {
                    if (x >= 1 && y >= 1 && x < width - 1 && y < heigth - 1)
                        cells.Add(new Cell(x, y));
                }
            }

            Cell startingCell = getCellAt(1, 1);
            walls.Add(startingCell);

            while (true)
            {
                Cell wall = (Cell)walls[random.Next(0, walls.Count)];

                processWall(wall);

                if (walls.Count <= 0)
                    break;
                if (--max < 0)
                    break;
            }

            foreach (Cell cell in cells)
            {
                if (cell.wall)
                {
                    map[cell.x, cell.y] = '#';
                }
                else
                {
                    map[cell.x, cell.y] = ' ';
                }
            }
            for (int y = 0; y < map.GetLength(1); y++)
            {
                for (int x = 0; x < map.GetLength(0); x++)
                {
                    if (x == 0 || y == 0 || x == map.GetLength(0) - 1 || y == map.GetLength(1) - 1)
                        map[x, y] = '#';
                }
            }
            return map;
        }

        void processWall(Cell cell)
        {
            int x = cell.x;
            int y = cell.y;
            if (cell.from == null)
            {
                if (random.Next(0, 2) == 0)
                {
                    x += random.Next(0, 2) - 1;
                }
                else
                {
                    y += random.Next(0, 2) - 1;
                }
            }
            else
            {

                x += (cell.x - cell.from.x);
                y += (cell.y - cell.from.y);
            }
            Cell next = getCellAt(x, y);
            if (next == null || !next.wall)
                return;
            cell.wall = false;
            next.wall = false;


            foreach (Cell process in getWallsAroundCell(next))
            {
                process.from = next;
                walls.Add(process);
            }
            walls.Remove(cell);
        }

        Cell getCellAt(int x, int y)
        {
            foreach (Cell cell in cells)
            {
                if (cell.x == x && cell.y == y)
                    return cell;
            }
            return null;
        }

        List<Cell> getWallsAroundCell(Cell cell)
        {
            List<Cell> near = new List<Cell>();
            List<Cell> check = new List<Cell>();

            check.Add(getCellAt(cell.x + 1, cell.y));
            check.Add(getCellAt(cell.x - 1, cell.y));
            check.Add(getCellAt(cell.x, cell.y + 1));
            check.Add(getCellAt(cell.x, cell.y - 1));

            foreach (Cell checking in check)
            {
                if (checking != null && checking.wall)
                    near.Add(checking);
            }
            return near;
        }

        public class Cell
        {
            public int x { get; set; }

            public int y { get; set; }

            public bool wall { get; set; }

            public Cell from { get; set; }

            public Cell(int x, int y)
            {
                this.x = x;
                this.y = y;
                this.wall = true;
            }
        }
    }
}

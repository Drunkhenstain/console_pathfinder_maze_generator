﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathFinder1
{
    class Generator
    {
        public int width { get; }
        public int height { get; }
        public byte wallsPercent { get; }
		static Random random= new Random();
		string[,] charMap;
        #region Generator()
        public Generator(int width, int height, byte wallsPercent)
        {
            if (width % 2 == 1 && width >= 5)
                this.width = width;
            else if (width > 5)
                this.width = width - 1;
            else
                this.width = 5;
            if (height % 2 == 1 && height >= 5)
                this.height = height;
            else if (height > 5)
                this.height = height - 1;
            else
                this.height = 5;
            if (wallsPercent <= 100)
                this.wallsPercent = wallsPercent;
            else
                this.wallsPercent = 100;
            random = new Random();
        }


		public Generator(int width, int height,int startX,int startY)
		{
			//for (int y = 1; y < charMap.GetLength(1) - 1; y++)
			//{
			//	for (int x = 1; x < charMap.GetLength(0) - 1; x++)
			//	{

			//	}
			//}			
			//	Start with a grid full of walls.
			//	Pick a cell, mark it as part of the maze.Add the walls of the cell to the wall list.
			//	While there are walls in the list:
			//		Pick a random wall from the list.If only one of the two cells that the wall divides is visited, then:
			//			Make the wall a passage and mark the unvisited cell as part of the maze.
			//			Add the neighboring walls of the cell to the wall list.
			//		Remove the wall from the list.
			this.width = width;
			this.height = height;
			charMap = new string[width, height];
			for (int y = 0; y < charMap.GetLength(1); y++)
			{
				for (int x = 0; x < charMap.GetLength(0); x++)						
				{
					charMap[x, y] = "#";
				}
			}
			List<PrimsPoint> walls = new List<PrimsPoint>();
			List<PrimsPoint> maze = new List<PrimsPoint>();

			PrimsPoint startPoint = new PrimsPoint(startX, startY, null);
			maze.Add(startPoint);
			charMap[startX, startY]= " ";

			for (int x = -1; x <= 1; x++)
				for (int y = -1; y <= 1; y++)
				{
					if (x == 0 && y == 0 || x != 0 && y != 0)
					{
						if (charMap[startPoint.X() + x,startPoint.Y() + y] == "#") 
							walls.Add(new PrimsPoint(startPoint.X() + x, startPoint.Y() + y, startPoint));
					}
				}


		}
		List<PrimsPoint> getNeighbours(PrimsPoint pointA)
		{
			List<PrimsPoint> neighbours = new List<PrimsPoint>();

				
			return neighbours;
		}


       
        #endregion

        public char[,] generateMap()
        {
            char[,] map = new char[height, width];
            for (int y = 0; y < height; y++)
                for (int x = 0; x < width; x++)
                {
                    if (y == 0 || y == height - 1 || x == 0 || x == width - 1 || y % 2 == 0 && x % 2 == 0)
                        map[y, x] = '#';
                    else if (random.Next(1, 101) <= wallsPercent)
                    {
                        int rnd = random.Next(1, 101);

                        if (rnd > 50)
                            map[y, x] = '+';
                        else if(rnd > 25)
                            map[y, x] = 'L';
                        else if (rnd > 0)
                            map[y, x] = '!';
                    }
                    else
                        map[y, x] = ' ';
                }
            map[1, 1] = ' ';                                      //topleft
            map[2, 1] = ' ';
            map[1, 2] = ' ';
            map[1, 3] = ' ';
            map[3, 1] = ' ';

            map[map.GetLength(0) - 2, 1] = ' ';                       //bottomleft
            map[map.GetLength(0) - 2, 2] = ' ';
            map[map.GetLength(0) - 3, 1] = ' ';

            map[1, map.GetLength(1) - 2] = ' ';                    //topright
            map[2, map.GetLength(1) - 2] = ' ';
            map[1, map.GetLength(1) - 3] = ' ';

            map[map.GetLength(0) - 2, map.GetLength(1) - 2] = ' ';     //bottomright
            map[map.GetLength(0) - 3, map.GetLength(1) - 2] = ' ';
            map[map.GetLength(0) - 2, map.GetLength(1) - 3] = ' ';
            map[map.GetLength(0) - 4, map.GetLength(1) - 2] = ' ';
            map[map.GetLength(0) - 2, map.GetLength(1) - 4] = ' ';

            return map;
        }

        //public char[,] generateItems()
        //{

        //}
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.IO;
using System.Diagnostics;

namespace PathFinder1
{
    class MainClass
    {
        static Stopwatch stopwatch = new Stopwatch();
        static Dictionary<Vertex, Vertex> predecessors = new Dictionary<Vertex, Vertex>();
        static Dictionary<Vertex, int> distance = new Dictionary<Vertex, int>();
        static List<Vertex> vertexes;
        static List<Edge> edges;
        static LinkedList<Vertex> finalpath;
        static char[,] map;
        public static void Main(string[] args)
        {
            PrimsGenerator prims = new PrimsGenerator();
            //stopwatch.Start();
            //map = prims.MazeIt( 99, 79);
            //stopwatch.Stop();
            //Console.WriteLine("Prims needed: " + stopwatch.ElapsedMilliseconds + " ms to Build");
            //Console.WriteLine("Press Enter");
            while (true)
            {
                Console.WriteLine("Ungerade Breite: ");
                string width = Console.ReadLine();
                Console.WriteLine("Ungerade Höhe: ");
                string heigth = Console.ReadLine();
                
                stopwatch.Start();
                map = prims.MazeIt(int.Parse(width), int.Parse(heigth));
                stopwatch.Stop();
                Console.WriteLine("Prims needed: " + stopwatch.ElapsedMilliseconds + " ms to Build");
                Console.WriteLine("Press Enter");

                CharMapGraphConverter testConvert = new CharMapGraphConverter(map);
                vertexes = testConvert.getVertexes();
                edges = testConvert.getEdges();
                Graph testGraph = new Graph(vertexes, edges);

                Console.WriteLine("PRESS ENTER TO INITIALIZE PATH FINDER");
                Console.ReadLine();
                Console.Clear();
                stopwatch.Start();
                PathFinder testFinder = new PathFinder(testGraph);
                stopwatch.Stop();
                Console.WriteLine("PathFinder needed: " + stopwatch.ElapsedMilliseconds + " ms to Build");
                Console.WriteLine("Press Enter To Build All Paths");
                Console.ReadLine();
                Console.Clear();
                stopwatch.Restart();
                testFinder.execute(new Vertex(map.GetLength(1) - 2, map.GetLength(0) - 2, 1));
                stopwatch.Stop();
                Console.WriteLine("PathFinder needed: " + stopwatch.ElapsedMilliseconds + " ms to Build Paths From Source");
                Console.WriteLine("PRESS ENTER TO DRAW MAP");
                Console.ReadLine();
                Console.Clear();
                for (int y = 0; y < map.GetLength(0); y++)
                {
                    for (int x = 0; x < map.GetLength(1); x++)
                    {
                        print(x, y, map[y, x].ToString());
                    }
                }
                Console.WriteLine("PRESS ENTER TO GET PATH");
                Console.ReadLine();
                Console.Clear();
                stopwatch.Restart();
                finalpath = testFinder.getPath(new Vertex(1, 1, 1));
                stopwatch.Stop();
                Console.WriteLine("PathFinder needed: " + stopwatch.ElapsedMilliseconds + " ms to Build shortest Path");
                Console.WriteLine("PRESS ENTER TO PRINT PATH");
                Console.ReadLine();
                Console.Clear();
                predecessors = testFinder.getPredecessors();
                distance = testFinder.getDistance();
                for (int y = 0; y < map.GetLength(0); y++)
                {
                    for (int x = 0; x < map.GetLength(1); x++)
                    {
                        print(x, y, map[y, x].ToString());
                    }
                }
                foreach (var step in finalpath)
                {
                    Thread.Sleep(70);
                    print(step.getX(), step.getY(), "@", ConsoleColor.Green);
                }
                Console.ReadLine();
                Console.Clear();
            }
        }
        static void print(int x, int y, string text)
        {
            Console.SetCursorPosition(x, y);
            Console.Write(text);
        }
        static void print(int x, int y, string text, ConsoleColor fgColor)
        {
            Console.ForegroundColor = fgColor;
            Console.SetCursorPosition(x, y);
            Console.Write(text);
            Console.ResetColor();
        }
    }
}

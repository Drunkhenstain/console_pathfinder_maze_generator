﻿using System;
namespace PathFinder1
{
	public class PrimsPoint
	{
		int posX;
		int posY;
		PrimsPoint parent;
		public PrimsPoint(int posX,int posY,PrimsPoint parent)
		{
			this.posX = posX;
			this.posY = posY;
			this.parent = parent;
		}
		public int X()
		{
			return posX;
		}

		public int Y()
		{
			return posY;
		}

		public string ID()
		{
			return posX + " " + posY;
		}

		public PrimsPoint P()
		{
			return parent;
		}
	}
}

﻿using System;
namespace PathFinder1
{
	public class Edge
	{
		string id;
		int weight;
		Vertex source;
		Vertex destination;


		public Edge(Vertex source, Vertex destination)
		{
            id = source.getID() + destination.getID();
			this.source = source;
			this.destination = destination;
			weight = 1+destination.getValue();
		}

		public Vertex getSource()
		{
			return source;
		}

		public Vertex getDestination()
		{
			return destination;
		}

		public int getWeight()
		{
			return weight;
		}
	}
}

﻿using System;
using System.Collections.Generic;
namespace PathFinder1
{
    public class CharMapGraphConverter
    {
        static Random random = new Random();
        List<Vertex> vertexes;
        List<Edge> edges;

        char[,] map;

        public CharMapGraphConverter(char[,] map)
        {
            vertexes = new List<Vertex>();
            edges = new List<Edge>();
            this.map = map;
            generateVertexes();
            generateEdges();
        }

        public List<Vertex> getVertexes()
        {
            return vertexes;
        }

        public List<Edge> getEdges()
        {
            return edges;
        }

        void generateVertexes()
        {
            for (int y = 0; y < map.GetLength(0); y++)
            {
                for (int x = 0; x < map.GetLength(1); x++)
                {

                    if (map[y, x] != '#' && map[y, x] != '.')
                    {
                        vertexes.Add(new Vertex(x, y, 1));
                    }
                }
            }
        }

        void generateEdges()
        {
            foreach (Vertex vertexA in vertexes)
            {
                foreach (Vertex vertexB in vertexes)
                {
                    if (vertexA.getX() == vertexB.getX() && vertexA.getY() == vertexB.getY() - 1)       //CheckTOP
                    {
                        edges.Add(new Edge(vertexA, vertexB));
                    }

                    if (vertexA.getX() == vertexB.getX() && vertexA.getY() == vertexB.getY() + 1)       //CheckBottom
                    {
                        edges.Add(new Edge(vertexA, vertexB));
                    }

                    if (vertexA.getX() == vertexB.getX() - 1 && vertexA.getY() == vertexB.getY())       //CheckLeft
                    {
                        edges.Add(new Edge(vertexA, vertexB));
                    }

                    if (vertexA.getX() == vertexB.getX() + 1 && vertexA.getY() == vertexB.getY())       //CheckRight
                    {
                        edges.Add(new Edge(vertexA, vertexB));
                    }
                }
            }
        }
    }
}

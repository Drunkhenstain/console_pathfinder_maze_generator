﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using FibonacciHeap;
using System.Diagnostics;

namespace PathFinder1
{
	public class PathFinder
	{
		static Stopwatch stopwatch= new Stopwatch();

		static Stopwatch stopwatch1 = new Stopwatch();

		static Stopwatch stopwatch2 = new Stopwatch();

		List<Vertex> nodes;
		List<Edge> edges;

		List<Vertex> settledNodes = new List<Vertex>();
		List<Vertex> unSettledNodes = new List<Vertex>();

		Dictionary<Vertex, Vertex> predecessors = new Dictionary<Vertex, Vertex>();
		Dictionary<Vertex, int> distance = new Dictionary<Vertex, int>();

		Vertex source;

		public PathFinder(Graph graph)
		{
			this.edges = graph.getEdges();
			this.nodes = graph.getVertexes();
		}

		public void execute(Vertex source)
		{
			this.source = source;
			distance.Add(source, 0);
			unSettledNodes.Add(source);

			while (unSettledNodes.Count > 0)
			{
				Vertex node = getMinimum(unSettledNodes);
				settledNodes.Add(node);
				unSettledNodes.Remove(node);
				findMinimalDistances(node);
				//Console.WriteLine("_________________________________________");
			}
			//Console.ReadLine();
		}

		void findMinimalDistances(Vertex node)
		{
			List<Vertex> adjacentNodes = getNeighbours(node);
			stopwatch1.Restart();
			foreach (Vertex target in adjacentNodes)
			{
				if (getShortestDistance(target) > getShortestDistance(target) + getDistance(node, target))
				{
					stopwatch.Restart();
					distance[target] = getShortestDistance(node) + getDistance(node, target);
					stopwatch.Stop();
					//Console.WriteLine(stopwatch.ElapsedTicks+"\tticks for distance update");
					stopwatch.Restart();
					predecessors[target] = node;
					stopwatch.Stop();
					//Console.WriteLine(stopwatch.ElapsedTicks + "\tticks for predecessor update");
					unSettledNodes.Add(target);
				}
			}
			stopwatch1.Stop();
			//Console.WriteLine(stopwatch1.ElapsedTicks + "\tticks for searching edges");
		}

		int getDistance(Vertex node, Vertex target)
		{
			int d = 1;
			int d2 = 1;
			foreach (Edge edge in edges)
			{
				if (edge.getSource().getID() == node.getID() && edge.getDestination().getID() == target.getID())
					d = edge.getWeight();
				if (edge.getSource().getID() == target.getID() && edge.getDestination().getID() == node.getID())
					d2 = edge.getWeight();
			}

			if (d < d2)
				return d;
			else
				return d2;
		}

		List<Vertex> getNeighbours(Vertex node)
		{
			List<Vertex> neighbours = new List<Vertex>();
			foreach (Edge edge in edges)
			{
				if ((edge.getSource().getID() == node.getID()) && !isSettled(edge.getDestination()))
				{
					neighbours.Add(edge.getDestination());
				}
				if (neighbours.Count == 3)
					break;
			}
			return neighbours;
		}

		Vertex getMinimum(List<Vertex> vertexes)
		{
			Vertex minimum = null;
			foreach (Vertex vertex in vertexes)
			{
				if (minimum == null)
					minimum = vertex;
				else if (getShortestDistance(vertex) <= getShortestDistance(minimum))
				{
					minimum = vertex;
				}
			}
			return minimum;
		}

		bool isSettled(Vertex vertex)
		{
			return settledNodes.Contains(vertex);
		}

		int getShortestDistance(Vertex destination)
		{
			stopwatch2.Restart();
			int d = int.MaxValue;
			foreach (var currentdistance in distance)
			{
				if (currentdistance.Key.getID() == destination.getID())
				{
					d = currentdistance.Value;
				}
			}
			stopwatch2.Stop();
			//Console.WriteLine(stopwatch2.ElapsedTicks + "\tticks for getting shortest Distance");

			if (d == int.MinValue)
				return int.MaxValue;
			else
				return d;
		}

		public Dictionary<Vertex, Vertex> getPredecessors()
		{
			return predecessors;
		}

		public Dictionary<Vertex, int> getDistance()
		{
			return distance;
		}

		public LinkedList<Vertex> getPath(Vertex target)
		{
			Stopwatch timer = new Stopwatch();
			LinkedList<Vertex> path = new LinkedList<Vertex>();
			Vertex step = target;
			path.AddFirst(step);

			timer.Start();
			while (step != source)
			{
				foreach (var stepto in predecessors)
				{
					if (stepto.Value.getID() == step.getID())
					{
						path.AddLast(stepto.Key);
						step = stepto.Key;
					}

					if (stepto.Key.getID() == step.getID())
					{
						path.AddLast(stepto.Value);
						step = stepto.Value;
					}

					if (timer.ElapsedMilliseconds > 3000)
						break;

					if (step == source)
						break;
				}
				if (timer.ElapsedMilliseconds > 3000)
					break;
			}
            List<Vertex> temppath = new List<Vertex>(path);

            for (int pos = 0; pos < temppath.Count - 1; pos++)
            {
                if (pos > 0 && pos < temppath.Count)
                {
                    if (temppath[pos - 1] == temppath[pos + 1])
                        temppath.RemoveAt(pos);
                }
            }
            for (int pos = 0; pos < temppath.Count - 1; pos++)
            {
                if (temppath[pos] == temppath[pos + 1])
                    temppath.RemoveAt(pos);
            }
            path = new LinkedList<Vertex>(temppath);
            return path;
		}
	}
}

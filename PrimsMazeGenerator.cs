u s i n g   S y s t e m ;  
  
  
 / / 	 	 	 p u b l i c   c l a s s   P r i m   {  
     	 	 	  
 / / 	 	 	   p u b l i c   s t a t i c   v o i d   m a i n ( S t r i n g [ ]   a r g s )   {  
 / / 	 	 	     / /   d i m e n s i o n s   o f   g e n e r a t e d   m a z e  
 / / 	 	 	     i n t   r   =   1 0 ,   c   =   1 0 ;  
     	 	 	  
 / / 	 	 	     / /   b u i l d   m a z e   a n d   i n i t i a l i z e   w i t h   o n l y   w a l l s  
 / / 	 	 	     S t r i n g B u i l d e r   s   =   n e w   S t r i n g B u i l d e r ( c ) ;  
 / / 	 	 	     f o r   ( i n t   x   =   0 ;   x   <   c ;   x + + )  
 / / 	 	 	       s . a p p e n d ( ' * ' ) ;  
 / / 	 	 	     c h a r [ ] [ ]   m a z   =   n e w   c h a r [ r ] [ c ] ;  
 / / 	 	 	     f o r   ( i n t   x   =   0 ;   x   <   r ;   x + + )   m a z [ x ]   =   s . t o S t r i n g ( ) . t o C h a r A r r a y ( ) ;  
     	 	 	  
 / / 	 	 	     / /   s e l e c t   r a n d o m   p o i n t   a n d   o p e n   a s   s t a r t   n o d e  
 / / 	 	 	     P o i n t   s t   =   n e w   P o i n t ( ( i n t ) ( M a t h . r a n d o m ( )   *   r ) ,   ( i n t ) ( M a t h . r a n d o m ( )   *   c ) ,   n u l l ) ;  
 / / 	 	 	     m a z [ s t . r ] [ s t . c ]   =   ' S ' ;  
     	 	 	  
 / / 	 	 	     / /   i t e r a t e   t h r o u g h   d i r e c t   n e i g h b o r s   o f   n o d e  
 / / 	 	 	     A r r a y L i s t   <   P o i n t   >   f r o n t i e r   =   n e w   A r r a y L i s t   <   P o i n t   >   ( ) ;  
 / / 	 	 	     f o r   ( i n t   x   =   - 1 ;   x   < =   1 ;   x + + )  
 / / 	 	 	       f o r   ( i n t   y   =   - 1 ;   y   < =   1 ;   y + + )   {  
 / / 	 	 	         i f   ( x   = =   0   & &   y   = =   0   | |   x   ! =   0   & &   y   ! =   0 )  
 / / 	 	 	           c o n t i n u e ;  
 / / 	 	 	         t r y   {  
 / / 	 	 	           i f   ( m a z [ s t . r   +   x ] [ s t . c   +   y ]   = =   ' . ' )   c o n t i n u e ;  
 / / 	 	 	         }   c a t c h   ( E x c e p t i o n   e )   {   / /   i g n o r e   A r r a y I n d e x O u t O f B o u n d s  
 / / 	 	 	           c o n t i n u e ;  
 / / 	 	 	         }  
 / / 	 	 	         / /   a d d   e l i g i b l e   p o i n t s   t o   f r o n t i e r  
 / / 	 	 	         f r o n t i e r . a d d ( n e w   P o i n t ( s t . r   +   x ,   s t . c   +   y ,   s t ) ) ;  
 / / 	 	 	       }  
     	 	 	  
 / / 	 	 	     P o i n t   l a s t   =   n u l l ;  
 / / 	 	 	     w h i l e   ( ! f r o n t i e r . i s E m p t y ( ) )   {  
     	 	 	  
 / / 	 	 	       / /   p i c k   c u r r e n t   n o d e   a t   r a n d o m  
 / / 	 	 	       P o i n t   c u   =   f r o n t i e r . r e m o v e ( ( i n t ) ( M a t h . r a n d o m ( )   *   f r o n t i e r . s i z e ( ) ) ) ;  
 / / 	 	 	       P o i n t   o p   =   c u . o p p o s i t e ( ) ;  
 / / 	 	 	       t r y   {  
 / / 	 	 	         / /   i f   b o t h   n o d e   a n d   i t s   o p p o s i t e   a r e   w a l l s  
 / / 	 	 	         i f   ( m a z [ c u . r ] [ c u . c ]   = =   ' * ' )   {  
 / / 	 	 	           i f   ( m a z [ o p . r ] [ o p . c ]   = =   ' * ' )   {  
     	 	 	  
 / / 	 	 	             / /   o p e n   p a t h   b e t w e e n   t h e   n o d e s  
 / / 	 	 	             m a z [ c u . r ] [ c u . c ]   =   ' . ' ;  
 / / 	 	 	             m a z [ o p . r ] [ o p . c ]   =   ' . ' ;  
     	 	 	  
 / / 	 	 	             / /   s t o r e   l a s t   n o d e   i n   o r d e r   t o   m a r k   i t   l a t e r  
 / / 	 	 	             l a s t   =   o p ;  
     	 	 	  
 / / 	 	 	             / /   i t e r a t e   t h r o u g h   d i r e c t   n e i g h b o r s   o f   n o d e ,   s a m e   a s   e a r l i e r  
 / / 	 	 	             f o r   ( i n t   x   =   - 1 ;   x   < =   1 ;   x + + )  
 / / 	 	 	               f o r   ( i n t   y   =   - 1 ;   y   < =   1 ;   y + + )   {  
 / / 	 	 	                 i f   ( x   = =   0   & &   y   = =   0   | |   x   ! =   0   & &   y   ! =   0 )  
 / / 	 	 	                   c o n t i n u e ;  
 / / 	 	 	                 t r y   {  
 / / 	 	 	                   i f   ( m a z [ o p . r   +   x ] [ o p . c   +   y ]   = =   ' . ' )   c o n t i n u e ;  
 / / 	 	 	                 }   c a t c h   ( E x c e p t i o n   e )   {  
 / / 	 	 	                   c o n t i n u e ;  
 / / 	 	 	                 }  
 / / 	 	 	                 f r o n t i e r . a d d ( n e w   P o i n t ( o p . r   +   x ,   o p . c   +   y ,   o p ) ) ;  
 / / 	 	 	               }  
 / / 	 	 	           }  
 / / 	 	 	         }  
 / / 	 	 	       }   c a t c h   ( E x c e p t i o n   e )   {   / /   i g n o r e   N u l l P o i n t e r   a n d   A r r a y I n d e x O u t O f B o u n d s  
 / / 	 	 	       }  
     	 	 	  
 / / 	 	 	       / /   i f   a l g o r i t h m   h a s   r e s o l v e d ,   m a r k   e n d   n o d e  
 / / 	 	 	       i f   ( f r o n t i e r . i s E m p t y ( ) )  
 / / 	 	 	         m a z [ l a s t . r ] [ l a s t . c ]   =   ' E ' ;  
 / / 	 	 	     }  
     	 	 	  
 / / 	 	 	     / /   p r i n t   f i n a l   m a z e  
 / / 	 	 	     f o r   ( i n t   i   =   0 ;   i   <   r ;   i + + )   {  
 / / 	 	 	       f o r   ( i n t   j   =   0 ;   j   <   c ;   j + + )  
 / / 	 	 	         S y s t e m . o u t . p r i n t ( m a z [ i ] [ j ] ) ;  
 / / 	 	 	       S y s t e m . o u t . p r i n t l n ( ) ;  
 / / 	 	 	     }  
 / / 	 	 	   }  
     	 	 	  
 / / 	 	 	   s t a t i c   c l a s s   P o i n t   {  
 / / 	 	 	     I n t e g e r   r ;  
 / / 	 	 	     I n t e g e r   c ;  
 / / 	 	 	     P o i n t   p a r e n t ;  
 / / 	 	 	     p u b l i c   P o i n t ( i n t   x ,   i n t   y ,   P o i n t   p )   {  
 / / 	 	 	         r   =   x ;  
 / / 	 	 	         c   =   y ;  
 / / 	 	 	         p a r e n t   =   p ;  
 / / 	 	 	       }  
 / / 	 	 	       / /   c o m p u t e   o p p o s i t e   n o d e   g i v e n   t h a t   i t   i s   i n   t h e   o t h e r   d i r e c t i o n   f r o m   t h e   p a r e n t  
 / / 	 	 	     p u b l i c   P o i n t   o p p o s i t e ( )   {  
 / / 	 	 	       i f   ( t h i s . r . c o m p a r e T o ( p a r e n t . r )   ! =   0 )  
 / / 	 	 	         r e t u r n   n e w   P o i n t ( t h i s . r   +   t h i s . r . c o m p a r e T o ( p a r e n t . r ) ,   t h i s . c ,   t h i s ) ;  
 / / 	 	 	       i f   ( t h i s . c . c o m p a r e T o ( p a r e n t . c )   ! =   0 )  
 / / 	 	 	         r e t u r n   n e w   P o i n t ( t h i s . r ,   t h i s . c   +   t h i s . c . c o m p a r e T o ( p a r e n t . c ) ,   t h i s ) ;  
 / / 	 	 	       r e t u r n   n u l l ;  
 / / 	 	 	     }  
 / / 	 	 	   }  
 / / 	 	 	 }  
  
  
 n a m e s p a c e   A p p l i c a t i o n  
 {  
 	 p u b l i c   c l a s s   P r i m s M a z e G e n e r a t o r  
 	 {  
 	 	 p u b l i c   P r i m s M a z e G e n e r a t o r ( )  
 	 	 {  
 	 	 }  
 	 }  
 }  
 